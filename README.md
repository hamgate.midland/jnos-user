# JNOS user files

This repository contains the files stored in `/home/jnos_system_account`

This account contains

* Scripts for cron jobs
* Executables for cron jobs
* Source files for those executables
* Work areas for cron jobs

The final result of these jobs is usually stored in the /jnos
directory tree, often in /jnos/public

---

